/*
投注球通用脚本
*/
cc.Class({
    extends: cc.Component,

    properties: {
       labDec:{
            default: null,
            type: cc.Label
        },

        labSelDec:{
            default: null,
            type: cc.Label
        },

        labMiss:{
            default: null,
            type: cc.Label
        },

        _ballType:0//0正常1红色2黄色
    },

    // use this for initialization
    onLoad: function () {
        
    },

    init: function(data) {
        this.labDec.string = data.num;
        this.labSelDec.string = data.num;
        this.setMissNum(data.miss)
    },

    //设置球数字颜色
    setNumColor:function(color){
        this.labDec.node.color = color;
    },

    //设置遗漏
    setMissNum:function(miss){
        if( miss == "" && miss != 0)
        {
            this.labMiss.node.active = false;
        }
        else
        {
            this.labMiss.node.active = true;
            this.labMiss.string = miss;
        }
    },

    //设置文字
    setNum:function(num){
        this.labDec.string = num;
    },

    //得到文字
    getNum:function(){
        return this.labDec.string;
    },

    //设置类型
    setType:function(type){
        this._ballType = type;
    },

    //得到球类型
    getType:function(){
        return this._ballType;
    },

});
