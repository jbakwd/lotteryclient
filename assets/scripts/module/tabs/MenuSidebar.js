cc.Class({
    extends: cc.Component,

    properties: {
        tabPrefab: cc.Prefab,
        container: cc.Node,
        highlight: cc.Node,
        _tabWidth: 0
    },

    // use this for initialization
    init (mainMenu,nameArry,x) {
        this.mainMenu = mainMenu;
        this.tabSwitchDuration = mainMenu.tabSwitchDuration;
        this._tabWidth = x;
        this.curTabIdx = 0;
        this.tabs = [];
        for (let i = 0; i < nameArry.length; ++i) {
            let tab = cc.instantiate(this.tabPrefab).getComponent('public_table_tab');
            this.container.addChild(tab.node);
            tab.init({
                sidebar: this,
                idx: i,
                name:nameArry[i],
                sizeX:x,   
            });
            this.tabs[i] = tab;
        }
        this.highlight.width = this._tabWidth;
        this.highlight.x = this.curTabIdx * this._tabWidth;
    },

    tabPressed (idx,state) {
        for (let i = 0; i < this.tabs.length; ++i) {
            let tab = this.tabs[i];
            if (tab.idx === idx) {
                cc.eventManager.pauseTarget(tab.node);
            } else if (this.curTabIdx === tab.idx) {
                cc.eventManager.resumeTarget(tab.node);
            }
        }
        this.curTabIdx = idx;
        let highlightMove = cc.moveTo(this.tabSwitchDuration, cc.p(this.curTabIdx * this._tabWidth)).easing(cc.easeQuinticActionInOut());
        this.highlight.stopAllActions();
        this.highlight.runAction(highlightMove);
        this.mainMenu.switchPanel(this.curTabIdx,state);
    }
});
