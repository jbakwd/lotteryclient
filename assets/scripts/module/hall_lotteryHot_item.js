/**
 * !#zh 推荐彩种组件
 * @information 开奖加奖
 */
cc.Class({
    extends: cc.Component,
    properties: {
        spLotteryStatus:{
            default: null,
            type: cc.Sprite
        },

        spLotteryBg:{
            default: null,
            type: cc.Sprite
        }
    },

    // use this for initialization
    init: function (data) {
        this.spLotteryStatus.spriteFrame = data.LotteryStatusFrame;
        this.spLotteryBg.spriteFrame = data.LotteryBgFrame;
    },

    /** 
    * 推荐彩种是否开奖或加奖
    * @method setState
    * @param {String} state
    */
    setState:function(state){
        this.spLotteryStatus.spriteFrame = state;
    }

});
