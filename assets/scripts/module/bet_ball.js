cc.Class({
    extends: cc.Component,

    properties: {
        labDec:{
            default: null,
            type: cc.Label
        },

        spBall:{
            default: null,
            type: cc.Sprite
        },

        _data:null,
    },

    // use this for initialization
    onLoad: function () {
        if(this._data == null)
            return; 
        this._data.frame == null ? this.spBall.node.active = false : this.spBall.spriteFrame = this._data.frame;  
        this._data.num == "" ? this.labDec.node.active = false : this.labDec.string = this._data.num;   
    },

    init:function(data){
       this._data = data;
    }

});
