/**
 * !#zh 记录(帐户明细)信息组件
 * @information 记录类型,时间，收入/支出金额 
 */
cc.Class({
    extends: cc.Component,

    properties: {
        labTime:{
            default: null,
            type: cc.Label
        },

        labMoney:{
            default: null,
            type: cc.Label
        },

        labWhere:{
            default: null,
            type: cc.Label
        },

        labWhereDec:{
            default: null,
            type: cc.Label
        },

        labDay:{
            default:null,
            type: cc.Label
        },

        labYear:{
            default:null,
            type:cc.Label
        },

        ndTime:{
            default:null,
            type:cc.Node
        },

        spWin: cc.Node,

        _data:null
    },

    // use this for initialization
    onLoad: function () {
        //cc.log('初始化与刷新',this._data.isShow)
        if(this._data != null)
        {
            this.labTime.string = this._data.time;
            this.labWhere.string = this._data.where;
            if(this._data.whereDce == "支出 ")
            {
                this.labWhereDec.node.color = cc.Color.RED;
                this.labMoney.node.color = cc.Color.RED;
                this.labWhereDec.string = this._data.whereDce;  
                this.labMoney.string = "-"+this._data.Money;
            }
            else
            {
                var color = new cc.Color(0, 185, 30);//绿色
                this.labWhereDec.node.color = color;
                this.labMoney.node.color = color;
                this.labWhereDec.string = this._data.whereDce;  
                this.labMoney.string = "+"+this._data.Money;
            }
            this.ndTime.active = this._data.isShow;
            
            if(this._data.isShow)
            {
                this.labYear.string = this._data.year;
                this.labDay.string = this._data.day;
            }
        }
    },

    /** 
    * 接收记录信息
    * @method init
    * @param {Object} ret
    */
    init:function(ret){
        this._data = ret;
    },

    /** 
    * 接收小游戏记录数据并显示
    * @method initRecord
    * @param {Object} data
    */
    initRecord: function(data){
        //cc.log('我的小游戏记录',data);
        if(data !=null){
            var month = parseInt(data.Month);
            var day = parseInt(data.Day);
            var strMonth = (month != 0) ? month + "月" : "";
            var strDay = (day != 0) ? day + "日" : ""; 
            var strMoney = data.Money.toString() + "元";
            if(strMonth == "" && strDay == "")
            {
                this.ndTime.active = false;
            }
            else
            {
                this.ndTime.active = true;
                this.labYear.string = strMonth;
                this.labDay.string = strDay;
            }

            if(data.type != 6)
            {
                if(data.type ==1 || data.type ==2){
                    this.labWhereDec.node.color = new cc.Color(0, 185, 30);
                }else{
                    this.labWhereDec.node.color = cc.Color.RED;
                }
                var str =data.type ==1 ? '充值' : data.type ==2 ? '领取' : data.type ==3 ? '赠送' : data.type ==4 ? '下注' : '投注';
                this.labWhereDec.string = str;  
                if(data.type ==1 || data.type ==2){
                    this.labMoney.node.color = new cc.Color(0, 185, 30);
                    this.labMoney.string = "+"+data.Money;
                }else if(data.type ==3 || data.type ==4 || data.type ==5){
                    this.labMoney.node.color = cc.Color.RED;
                    this.labMoney.string = "-"+data.Money;    
                }
                this.labWhere.string = data.LotteryName;
            }
            else
            {
                var color = new cc.Color(0, 185, 30);//绿色
                this.labWhereDec.node.color = color;
                this.labMoney.node.color = color;
                this.labWhereDec.string = '中奖';  
                this.labMoney.string = "+"+data.Money;
                this.labWhere.string = data.LotteryName;
                this.spWin.active =true; //显示中奖图片 
            }
            this.labTime.string = '时间：'+ data.Time.slice(8,10)+':'+data.Time.slice(10,12)+':'+data.Time.slice(12,data.Time.length);
        }
    }

});
