/**
 * !#zh 购彩订单组件
 * @information 彩种，订单状态，期次，日期，订单类型（跟单/普通订单）
 */
cc.Class({
    extends: cc.Component,

    properties: {
        labLotteryName:cc.Label,
        labState:cc.Label,
        labMoney:cc.Label,
        labMonth:cc.Label,
        labDay:cc.Label,
        labType:cc.Label,
        labSign:cc.Node,
        labIsuseNum:cc.Label,
        spWin:cc.Node
    },

    // use this for initialization
    onLoad: function () {

    },

    /** 
    * 接收购彩订单信息并初始化显示
    * @method init
    * @param {Object} data
    */
    init: function(data){
        var month = parseInt(data.Month);
        var day = parseInt(data.Day);
        var strMonth = (month != 0) ? month + "月" : "";
        var strDay = (day != 0) ? day + "日" : ""; 
        var strMoney = data.Money.toString() + " 元";
        if(strMonth == "" && strDay == "")
        {
            this.labSign.active = false;
        }
        else
        {
            this.labSign.active = true;
        }
        if(data.State == 14)
        {
            this.labState.node.active = false;
            this.spWin.active = true;
        }
        else
        {
            this.labState.node.active = true;
            this.spWin.active = false;
            this.labState.string = LotteryUtils.getOrderStatusByStatus(data.State);
        }


        this.labType.string = data.type;
        this.labLotteryName.string = data.LotteryName;
        this.labMoney.string = strMoney;
        this.labMonth.string = strMonth;
        this.labDay.string = strDay;
        this.labIsuseNum.string = data.IsuseNum == null ? "":data.IsuseNum;
    }
    
});
