/**
 * !#zh 快3走势图开奖组件
 * @information 期次，开奖号码，各值，大小，单双
 */
cc.Class({
    extends: cc.Component,

    properties: {
        labIssue:{
            default:null,
            type:cc.Label
        },

        labNum1:{
            default:null,
            type:cc.Label
        },

        labNum2:{
            default:null,
            type:cc.Label
        },

        labNum3:{
            default:null,
            type:cc.Label
        },

        labNum4:{
            default:null,
            type:cc.Label
        },

        spBgColor:{
            default:null,
            type:cc.Node
        },

        _data:null,
        _itemId: null
    },

    // use this for initialization
    onLoad: function () {
        if(this._data != null)
        {
            this.spBgColor.color = this._data.color;
            var isus =  this._data.Isuse.toString();
            var isuseStr = isus.substring(isus.length-2,isus.length) + "期";
            this.labIssue.string = isuseStr;
            var numStr = "";
            for(var i=0;i<this._data.num1.length;i++)
            {
                numStr += this._data.num1[i]+" ";
            }
            this.labNum1.string = numStr;
            this.labNum2.string = this._data.num2;
            this.labNum3.string = this._data.num3;
            this.labNum4.string = this._data.num4;
        }
    },

    /** 
    * 接收走势图开奖信息
    * @method init
    * @param {Object} data
    */
    init:function(data){
        this._data = data;
    },

    /** 
    * 刷新走势图开奖信息
    * @method updateData
    * @param {Object} data
    */
    updateData:function(data){
        this._data = data;
        if(data != null)
        {
            var isus =  this._data.Isuse.toString();
            var isuseStr = isus.substring(isus.length-2,isus.length) + "期";
            this.labIssue.string = isuseStr;
            var numStr = "";
            for(var i=0;i<this._data.num1.length;i++)
            {
                numStr += this._data.num1[i]+" ";
            }
            this.labNum1.string = numStr;
            this.labNum2.string = this._data.num2;
            this.labNum3.string = this._data.num3;
            this.labNum4.string = this._data.num4;
        }
    },

    /** 
    * 接收走势图开奖列表id
    * @method onItemIndex
    * @param {Number} i
    */
    onItemIndex: function(i){
        this._itemId = i;
    }

});
